package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodeToNode.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        if (hasNode(node)) {
            return;
        }
        nodeToNode.put(node, new HashSet<>());
    }

    @Override
    public void removeNode(V node) {
        if (!hasNode(node)) {
            return;
        }
        Set<V> removingSet = getNeighbourhood(node);
        nodeToNode.remove(node);
        for (V nodes: removingSet) {
            Set<V> removeNodeSet = nodeToNode.get(nodes);
            removeNodeSet.remove(node);
            nodeToNode.put(nodes, removeNodeSet);
        }
    }

    @Override
    public void addEdge(V u, V v) {
        if (!hasNode(u) || !hasNode(v)) {
            throw new IllegalArgumentException("You can't add an edge to a node that doesn't exist on the graph.");
        }
        Set<V> newSetu = nodeToNode.get(u);
        Set<V> newSetv = nodeToNode.get(v);
        newSetu.add(v);
        newSetv.add(u);
        nodeToNode.put(u, newSetu);
        nodeToNode.put(v, newSetv);
    }

    @Override
    public void removeEdge(V u, V v) {
        if (!hasNode(u) || !hasNode(v)) {
            throw new IllegalArgumentException("You can't remove an edge to a node that doesn't exist on the graph.");
        }
        Set<V> newSetu = nodeToNode.get(u);
        Set<V> newSetv = nodeToNode.get(v);
        newSetu.remove(v);
        newSetv.remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        return nodeToNode.containsKey(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}

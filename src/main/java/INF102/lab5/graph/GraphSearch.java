package INF102.lab5.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        int stop = graph.size();
        int count = 0;
        Set<V> beenThere = new HashSet<>();
        boolean answer = false;
        answer = connectionSearch(count, stop, beenThere, answer, u, v);
        return answer;
    }

    private boolean connectionSearch(int count, int stop, Set<V> beenThere, boolean answer, V startNode, V endNode) {
        if (answer == true) {
            return answer;
        }
        if (count >= stop) {
            answer = false;
            return answer;
        }
        beenThere.add(startNode);
        if (graph.hasEdge(startNode, endNode)) {
            answer = true;
            return answer;
        }
        for (V nodes : graph.getNeighbourhood(startNode)) {
            if (!beenThere.contains(nodes)) {
                count++;
                if (connectionSearch(count, stop, beenThere, answer, nodes, endNode)) {
                answer = true;
                return answer;
            }
            }
        }  
        return answer;
    }

}
